# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from .models import Post

def blog(request):
    queryset = Post.objects.all()
    context = {
        "posts" : queryset, 
    }
    return render(request, "blog.html", context)