# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=200)
    body = body = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)


    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ["-timestamp"]