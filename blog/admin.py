# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from models import Post
from django.contrib import admin

class BlogPostAdmin(admin.ModelAdmin):
    list_display = ["title","body","timestamp"]

admin.site.register(Post, BlogPostAdmin)