from django.contrib import admin
from models import HomeContent, AboutPost, FaqPost, SpeedFitnessPost, KavitationPost

class HomeContentAdmin(admin.ModelAdmin):
    list_display = ["title", "body","date"]


class AboutPostAdmin(admin.ModelAdmin):
    list_display = ["title", "body","date"]

class FaqPostAdmin(admin.ModelAdmin):
    list_display = ["question","answer"]

class SpeedFitnessAdmin(admin.ModelAdmin):
    list_display = ["title","description"]

class KavitationAdmin(admin.ModelAdmin):
    list_display = ["title","description"]    

admin.site.register(HomeContent, HomeContentAdmin)
admin.site.register(AboutPost, AboutPostAdmin)  
admin.site.register(FaqPost, FaqPostAdmin)
admin.site.register(SpeedFitnessPost, SpeedFitnessAdmin)
admin.site.register(KavitationPost, KavitationAdmin)