# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models
'''
This class is for the Content on te Homepage
the last entry is visible on the page
'''
class HomeContent(models.Model):
    title = models.CharField(max_length=140)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True, auto_now=False)
    author = models.CharField(max_length=140)

    def __unicode__(self):
        return self.title

'''
this lass is for the content on the about page.
The last entry is visible
'''
class AboutPost(models.Model):
    title = models.CharField(max_length=140)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True, auto_now=False)
    author = models.CharField(max_length=140)

    def __unicode__(self):
        return self.title

'''
This class is for the frequently asked qeustions.
All entry is visible
'''
class FaqPost(models.Model):
    question = models.TextField()
    answer = models.TextField()
    date = models.DateTimeField(auto_now_add=True, auto_now=False)
    author = models.CharField(max_length=140)

    def __unicode__(self):
        return self.question

'''
This class is the description of speedfitness
Last entry is visible
'''
class SpeedFitnessPost(models.Model):
    title = models.CharField(max_length=140)
    description = models.TextField()
    date = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __unicode__(self):
        return self.title   


class KavitationPost(models.Model):
    title = models.CharField(max_length=140)
    description = models.TextField()
    date = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __unicode__(self):
        return self.title    
