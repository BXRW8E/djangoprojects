from django.conf.urls import include, url
from django.contrib import admin
import views
from django.conf.urls import handler404

handler404 = 'index.views.error404'

urlpatterns = [
    #url(r'^$', views.index, name='index'),
    url(r'^$', views.indexList, name='index'),
    url(r'^kavitacio/$', views.kavitacio, name='kavitacio'),
    url(r'^speedfitness/$', views.speedfitness, name='speedfitness'),
    url(r'^kapcsolat/$', views.contact, name='contact'),
    url(r'^araink/$', views.pricelist, name='pricelist'),
    url(r'^gyakori_kerdesek/$', views.faq, name='faq'),
   # url(r'^rolunk/$', views.about, name='about'),
    url(r'^rolunk/$', views.aboutList, name='about'),
    url(r'^sitemap\.xml$', views.sitemap, name='sitemap'),
]
