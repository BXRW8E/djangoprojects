# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0002_aboutpost'),
    ]

    operations = [
        migrations.CreateModel(
            name='FaqPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.TextField()),
                ('answer', models.TextField()),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('author', models.CharField(max_length=140)),
            ],
        ),
    ]
