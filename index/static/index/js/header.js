/* function showWidth(ele, w) {
        $("w").text("The width for the " + ele + " is " + w + "px.");
    }
    function showWidthLeft(ele, w) {
        $("wl").text("The width for the " + ele + " is " + w + "px.");
    }
    function showWidthRight(ele, w) {
        $("wr").text("The width for the " + ele + " is " + w + "px.");
    }
    */

    /**
    * if windowwidth is less then 700px menu and
    * body content sizes will be sets
    */
    function setSizes() {
        var windowWidth = $(window).width();
        var leftWidth = $("div.left-side").width();
        var rightWidth = windowWidth - leftWidth;
        //showWidth( "window", windowWidth );
        //showWidthLeft( "left", leftWidth );
        //showWidthRight( "right", rightWidth );

        if (windowWidth < 700) {
            $("div.left-side").css("width", "100vw");
            $("div.left-side").css("max-width", "");
            //$("div.dropdown-content").css("width", "inherit");
            $("div.menu").css("text-align", "center");
            $("div.right-side").css("width", "97%");
            $("div.facebook-container").css("padding-left", "15vw");
            if ($("div.nav-list").length > 0) {
                $("div.nav-list").css("width", "100vw");
                //$("div.nav-list a").css("text-align", "center");
            }
            if ($("div#map").length > 0) {
                $("div#map").css("width", "100vw");
                $("div#map").css("height", "300px");
            }
        }
        else {
            $("div.left-side").css("width", "300px"); //20% volt
            //$("div.dropdown-content").css("width", "inherit");
            $("div.menu").css("text-align", "left");
           // $("div.right-side").css("width", "60vw");
           $("div.right-side").css("width", "calc(100vw - 360px)");
           $("div.left-side").css("max-width", "350px");
            $("div.facebook-container").css("padding-left", "20px");
            if ($("div.nav-list").length > 0) {
                $("div.nav-list").css("width", "11vw");
                //  $("div.nav-list").css("text-align", "left");
            }
            if ($("div#map").length > 0) {
                $("div#map").css("width", "40vmax");
                $("div#map").css("height", "30vw");
            }
        }
    }

    /**
    * change the arrow in the menu on click
    */
    $('.drop-click').click(function () {
        $(this).find('span').toggleClass('glyphicon glyphicon-chevron-down').toggleClass('glyphicon glyphicon-chevron-left');
    });
    /**
    * arrow wil be set in the dropdow menu
    */
    function showDropDown() {
        var state = 0;

        $("li.drop-click").click(function () {
            state ^= 1;

            if (state == 1) {
                $("div.dropdown-content").css("display", "block");
            }
            else {
                $("div.dropdown-content").css("display", "none");
            }
        })
    }

    /**
    *if resize event appears setSizes function wil be called
    */
    $(window).resize(function () {
        setSizes();
    });

    /**
    * If html page finishes loading
    */
    $(document).ready(function () {
        setSizes();
        showDropDown();
    });

    /**
    * When the user scrolls down 20px from the top of the document, show the button
    */
    window.onscroll = function () { scrollFunction() };

    /**
    *@brief: scroll utton appears if you are scrolling down
    */
    function scrollFunction() {
        if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 400) {
            document.getElementById("goTop").style.display = "block";
        } else {
            document.getElementById("goTop").style.display = "none";
        }
    }

    /**
    * When the user clicks on the button, scroll to the top of the document
    */
    function topFunction() {
        // document.body.scrollTop = 0; // For Chrome, Safari and Opera 
        //document.documentElement.scrollTop = 0; // For IE and Firefox
        $('html, body').animate({ scrollTop: $(".tr-2").offset().top - 90 }, 300);
    }