# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.contrib.sitemaps import Sitemap
from django.http import Http404
from models import HomeContent, AboutPost, FaqPost, SpeedFitnessPost, KavitationPost
import datetime

contact = {"address":["Cím","1039 Budapest, Üteg utca 11/b. "], "open": ["Nyitvatartás:", ]}

'''
@param request is the URL request
@return the main site 
'''
def index(request):
    return render(request, 'index/home.html')


def indexList(request):
    
   # return redirect('/rolunk/')

    queryset = HomeContent.objects.all().order_by('-date')[:1]
    context = {
        "posts": queryset,
    }
    return render(request, "index/home.html", context)


'''
@param request is the URL request
@return the speedfitness site
this site contains information about mia bodytec machine 
'''
def speedfitness(request):
    queryset = SpeedFitnessPost.objects.all().order_by('-date')[:1]
    context = {
        "speedfitness_posts": queryset,
    }
    return render(request, 'index/speedfitness.html', context)

'''
@param request is the URL request
@return the kavitacio site
this site contains information about the kavitacio machine 
'''
def kavitacio(request):
    queryset = KavitationPost.objects.all().order_by('-date')[:1]
    context = {
        "kavitation_posts": queryset,
    }
    return render(request, 'index/kavitacio.html', context)

'''
@param request is the URL request
@return the contact site 
'''
def contact(request):
    return render(request, 'index/contact.html')

'''
@param request is the URL request
@return the pricelsit site 
'''
def pricelist(request):
    return render(request, 'index/pricelist.html', {
    'sprice': [('2 500', 'Próbaedzés'), ('3 500', '"Happy Weekend" (1 alkalom)'),
    ('5 000', '1 alkalom'), ('9 900', '"Warm up" bérlet (3 alkalom)'),
    ('26 000', '6 almalmas bérlet'), ('29 900', 'Havi bérlet'),
    ('38 000', '"Happy hour" bérlet (10 alkalom)'), ('49 000', '12 alkalmas bérlet')],

    #'kvprice': [('1 000', '1 alkalom'), ('3 000', '3 alkalom'), ('6 000', '6 alkalom')]
    })

'''
@param: request the URL page
@return: returns the 'Ellenjavallatok' site
'''
def faq(request):
    queryset = FaqPost.objects.all().order_by('date')[:50]
    context = {
        "faq_posts": queryset,
    }
    return render(request, 'index/faq.html',context)

'''
@param: request the URL page
@return: returns the Error404 site
'''
def error404(request):
    return render(request, 'index/error404.html')

'''
@param: request the URL page
@return: returns the Error404 site
'''
def about(request):
    return render(request, 'index/about.html', {'title': 'Speedfintess/13 | R\ólunk' })


def aboutList(request):
    queryset = AboutPost.objects.all().order_by('-date')[:1]
    context = {
        "about_posts": queryset,
    }
    return render(request, "index/about.html", context)

'''
@param: request the URL page
@return: the blog page
'''
def blog(request):
    return render(request, "index/blog.html")

def sitemap(request):
    return render(request, "index/sitemap.xml")

