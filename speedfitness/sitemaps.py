#from django.conf.urls import *
from django.urls import reverse
#from speedfitness.urls import urlpatterns as homeUrls
from django.core.urlresolvers import reverse
from django.contrib.sitemaps import Sitemap
#from blog.models import Post

class StaticSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

     # The below method returns all urls defined in urls.py file
    def items(self):
       # for url in urlpatterns:
        #    mylist.append('home:'+url.name) 
        return ['rolunk', 'speedfitness', 'kavitacio', 'kapcsolat', 'araink', 'blog', 'galeria', 'kapcsolat', 'gyakori_kerdesek']
      #  return mylist

    def location(self, item):
        return reverse(item)